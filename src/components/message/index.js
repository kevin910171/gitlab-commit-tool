import component from './component.vue'

import Vue from 'vue'

const MessageConstructor = Vue.extend(component)
MessageConstructor.prototype.close = function () {
  this.visible = false
  this.mutationObserver.observe(this.$el, { attributes: true })
  messagePool.length && message.next(messagePool.pop())
}

const messagePool = []

const message = (() => {
  const create = () => new MessageConstructor().$mount()

  const next = instance => {
    const getDuration = (text) => {
      // 中文由字數判斷長度 其他文字由空白個數判斷長度
      const isChinese = /[\u4E00-\u9FFF]/.test(text)
      const length = isChinese ? text.replace(/[ \u3000\u3001-\u303F0-9]/g, '').length : text.split(' ').length
      const readSpeedPerSecond = 200 / 60
      const minDuration = 3000
      const dynamicDuration = length / readSpeedPerSecond * 1500 + 500
      return dynamicDuration > minDuration ? dynamicDuration : minDuration
    }
    instance.timer = setTimeout(() => instance.close(), getDuration(instance.text))
  }

  const append = instance => {
    instance && messagePool.unshift(instance)
    document.body.prepend(instance.$el)
    Vue.nextTick(() => {
      instance.visible = true
      messagePool.length === 1 && message.next(instance)
    })
  }

  const remove = event => event.$destroy()

  return {
    create,
    next,
    remove,
    append,
  }
})()

const action = (option) => {
  const instance = message.create()
  clearTimeout(instance.timer)
  instance.text = typeof (option) === 'string' ? option : option.text
  if (typeof (option) === 'object') {
    instance.type = option.type || 'success'
    instance.position = {
      X: option.position.X,
      Y: option.position.Y,
    }
  }
  instance.timeout = option.timeout || 2000
  instance.mutationObserver = new MutationObserver(
    mutations => mutations.forEach(
      mutation => {
        if (instance && mutation.target.style.display === 'none') {
          instance.mutationObserver.disconnect()
          message.remove(instance)
        }
      },

    ),
  )
  message.append(instance)
}

export default action
