import storage from '@/utils/storage'
import dayjs from 'dayjs'

export default {
  namespaced: true,
  state: {
    active: {},
    list: [],
  },
  mutations: {
    setTokenActive (state, active) {
      state.active = active
      storage.local.set('token', { list: state.list, active })
    },
    setTokenList (state, list) {
      state.list = list
      storage.local.set('token', { active: state.active, list })
    },
  },
  actions: {
    getToken ({ commit }) {
      const obj = storage.local.get('token')

      if (!obj) return

      if (!obj.active.token) obj.active = obj.list[0]

      commit('setTokenList', obj.list)
      commit('setTokenActive', obj.active)
    },
    updateList ({ state, commit }, data) {
      const { token, note, date } = data
      const isExist = state.list.find(node => node.token === token)
      if (isExist) return Promise.reject()
      const item = { token, note, date, createDate: dayjs().format('YYYY-MM-DD') }
      const newList = [...state.list, item]
      commit('setTokenList', newList)
      if (!state.active) commit('setTokenActive', item)
      return Promise.resolve()
    },
    updateActive ({ commit }, data) {
      console.log('data', data.token)

      commit('setTokenActive', data)
    },
  },
}
