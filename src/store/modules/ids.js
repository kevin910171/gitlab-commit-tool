import storage from '@/utils/storage'
import dayjs from 'dayjs'

export default {
  namespaced: true,
  state: {
    id: {
      group: [],
      user: [],
    },
  },
  mutations: {
    setIdList (state, list) {
      state.id = list
    },
  },
  actions: {
    getIdList ({ commit }) {
      const list = storage.local.get('ids')
      if (!list) return
      commit('setIdList', list)
    },
    updateIdList ({ state, commit }, data) {
      const { id, note, type } = data
      const isExist = state.id[type].find(node => node.id === id)
      if (isExist) return Promise.reject()
      const item = { id, note, createDate: dayjs().format('YYYY-MM-DD') }
      const newList = { ...state.id, [type]: [...state.id[type], item] }
      commit('setIdList', newList)
      return Promise.resolve()
    },
  },
}
