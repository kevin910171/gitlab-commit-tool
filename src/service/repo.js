import service from './config'
import dayjs from 'dayjs'
const repoService = {
  branches: ({ project }) => {
    if (isNaN(Number(project))) project = encodeURIComponent(project)
    return service({
      methods: 'get',
      url: `/projects/${project}/repository/branches`,
      isPrivateToken: true,
    })
  },
  commits: ({ project, branch, time: [since, until] }) => {
    since = !since ? new Date(new Date().getTime() - (90 * 24 * 60 * 60 * 1000)) : dayjs(since).toDate()
    until = !until ? new Date(new Date().getTime()) : dayjs(until).toDate()
    if (isNaN(Number(project))) project = encodeURIComponent(project)
    return service({
      methods: 'get',
      url: `/projects/${project}/repository/commits`,
      params: {
        since,
        until,
        ref_name: branch,
        per_page: 100,
      },
      isPrivateToken: true,
    })
  },
}
export default repoService
