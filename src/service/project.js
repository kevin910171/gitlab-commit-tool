import service from './config'
const projectService = {
  groups: ({ group }) => {
    if (isNaN(Number(group))) group = encodeURIComponent(group)
    return service({
      methods: 'get',
      url: `/groups/${group}/projects`,
      isPrivateToken: true,
    })
  },
  users: ({ userID }) => service({
    methods: 'get',
    url: `/users/${userID}/projects`,
    isPrivateToken: true,
  }),
}
export default projectService
